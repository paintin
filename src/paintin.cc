// Paintin' -- zoom video tool
// (c) 2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <cassert>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <vector>

struct cell;
struct edge;

struct edge
{
  int id;
  uint64_t count;

	edge() = default; // POD
	edge(const edge &a) = default; // POD
	edge(edge &&a) = default; // POD
	edge &operator=(const edge &a) = default; // POD
	edge &operator=(edge &&a) = default; // POD
	~edge() = default; // POD;
  
  edge(int id, uint64_t count) : id(id), count(count) { };
};

bool operator==(const edge &a, const edge &b)
{
  return a.id == b.id && a.count == b.count;
}

std::istream &operator>>(std::istream &in, edge &e)
{
  return in >> e.id >> e.count;
}

struct cell
{
  edge top, bottom;

	cell() = default; // POD
	cell(const cell &a) = default; // POD
	cell(cell &&a) = default; // POD
	cell &operator=(const cell &a) = default; // POD
	cell &operator=(cell &&a) = default; // POD
	~cell() = default; // POD;
};

std::istream &operator>>(std::istream &in, cell &c)
{
  return in >> c.top >> c.bottom;
}

struct zoom
{
  std::vector<cell> rules;
};

std::istream &operator>>(std::istream &in, zoom &z)
{
  cell c;
  while (! in.eof())
  {
    in >> c;
    if (! in.eof())
    {
      z.rules.push_back(c);
    }
  }
  return in;
}

bool can(const edge &from, const cell &to)
{
  return from.id == to.top.id && (from.count % to.top.count) == 0;
}

edge next(const edge &from, const cell &to)
{
  assert(can(from, to));
  uint64_t count = to.bottom.count * from.count / to.top.count;
  edge after{ to.bottom.id, count };
  return after;
}

bool can(const edge &from, const zoom &tos)
{
  for (const auto &to : tos.rules)
  {
    if (can(from, to))
    {
      return true;
    }
  }
  return false;
}

edge next(const edge &from, const zoom &tos)
{
  assert(can(from, tos));
  for (const auto &to : tos.rules)
  {
    if (can(from, to))
    {
      return next(from, to);
    }
  }
  throw std::invalid_argument("no next edge in zoom");
  abort();
}

std::ostream &operator<<(std::ostream &out, const edge &e)
{
  const int width = 80;
  const double aspect = 2;
  const int height = ceil(width / aspect / e.count);
  for (int y = 0; y < height; ++y)
  {
    for (int c = 0; c < width; ++c)
    {
      double d = fmod(c / (double) width * e.count, 1) / e.count * width;
      if (fabs(d) < 1)
      {
        out << char(y == 0 ? '+' : '|');
      }
      else
      {
        out << char(y == 0 ? '-' : 'a' + e.id);
      }
    }
    out << std::endl;
  }
  return out;
}

std::ostream &operator<<(std::ostream &out, const zoom &z)
{
  const edge e0 = { 0, 1 };
  edge e = { 0, 1 };
  while (can(e, z))
  {
    out << e;
    e = next(e, z);
    if (e == e0)
    {
      break;
    }
  }
  return out;
}

int main(int argc, char **argv)
{
  zoom z;
  std::cin >> z;
  std::cout << z;
  return 0;
}
