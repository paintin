-- Paintin' -- zoom video tool
-- (c) 2021 Claude Heiland-Allen
-- SPDX-License-Identifier: AGPL-3.0-only

import Control.Monad (guard)
import Data.Fixed (mod')
import Data.List (inits, tails)
import Data.Maybe (mapMaybe)

type Thing = Int

bra, ket :: Thing
bra = -1
ket = -2

things :: [(Char, Thing)]
things = ("()" ++ ['a' .. 'z']) `zip` ([bra, ket] ++ [0..])

names :: [(Thing, Char)]
names = map swap things

swap :: (a, b) -> (b, a)
swap (a, b) = (b, a)

for :: [a] -> (a -> b) -> [b]
for = flip map

type Rules = [([Thing], [Thing])]

apply :: Rules -> [Thing] -> [[Thing]]
apply _ [] = [[]]
apply rules term = do
  (pattern, replace) <- rules
  (match, rest) <- inits term `zip` tails term
  guard (match == pattern)
  let remainder = apply rules rest
  (replace ++) `fmap` remainder

choose :: [[Thing]] -> [Thing]
choose = head

main' :: String -> String
main' s =
  let seed = [0]
      rules
        = map (\[pattern,replace] -> (pattern, replace))
        . map (map (mapMaybe (`lookup` things)))
        . map words
         . lines
        $ s
  in    ( unlines
        . concatMap (boxed 80 2)
        . map (mapMaybe (`lookup` names))
        . (seed :)
        . takeWhile (/= seed)
        . tail
        . iterate (choose . apply rules)
        $ seed
        )

boxed :: Int -> Double -> String -> [String]
boxed width aspect s = case map (\w -> (length w, head w)) . words . map space $ s of
  boxes@((count, name):_) ->
    let n = length boxes * count
        height :: Int
        height = ceiling (f width / aspect / f n)
    in  for [0..height - 1] $ \y ->
          for [0..width - 1] $ \x ->
              case ((f x / f width * f n) `mod'` 1) / f n * f width of
                d | d < 1 -> if y == 0 then '+' else '|'
                _  -> if y == 0 then '=' else name
  _ -> []
  where
    f :: Int -> Double
    f = fromIntegral
    space '(' = ' '
    space ')' = ' '
    space c = c

main :: IO ()
main = interact main'
