---
title: "Paintin'"
author: Claude Heiland-Allen
...

# Paintin'

Zoom video tool.

<https://mathr.co.uk/paintin>

## Concept

See [examples](examples/).

Eventually each letter will correspond to an image that tiles seamlessly
with its neighbours in the doubling (or tripling, etc, mix and match
should be possible here) grid.

Needs suitable detail reduction in smaller mipmap levels so that things
like line width will remain visually constant when shrinking.  Then
re-project the exponential strip into a zoom video.  Eventually draw a
tileset by hand and scan it.  Later maybe animated tilesets.

Currently it uses a vaguely L-system like specification.  Order of rules
matters, selecting the shortest result out of all possible variants is
too inefficient.  Seed is fixed to "a", when the seed is reached again
it stops / loops.

An aim is for this all to be computationally efficient (target is
roughly laptop hardware from mid-late 2000s).

## Source

Browse at <https://code.mathr.co.uk/paintin>

Obtain via

```
git clone https://code.mathr.co.uk/paintin.git
```

## Legal

Copyright (C) 2021 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>

---
<https://mathr.co.uk>
