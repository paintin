# Paintin' -- zoom video tool
# (c) 2021 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

TARGETS = \
examples/complex.txt \
examples/complex.txt.gz \
examples/simple.txt \
examples/simple.txt.gz \
index.html \
index.html.gz \
paintin \
paintin.css.gz \

# blank line above

all: $(TARGETS)

#paintin: src/paintin.cc
#	g++ -std=c++20 -Wall -Wextra -pedantic -o paintin src/paintin.cc -ggdb

paintin: src/paintin.hs
	ghc -Wall -Wextra -o paintin src/paintin.hs -O2

clean:
	rm -f $(TARGETS)

examples/%.txt: examples/%.paintin paintin
	./paintin < $< > $@

index.html: README.md
	pandoc $< -s --toc --css paintin.css -o $@

%.gz: %
	gzip -9 -k -f $<

.PHONY: clean
.SUFFIXES:
